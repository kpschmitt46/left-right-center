# -*- coding: utf-8 -*-

from playerClass import player

class gameplay:
    
    def __init__(self, ni):
        """
        Initialize game play.  Create and link players.
        
        Inputs:
        ni (int) : number of players
        """
        
        if ni < 2:
            return ValueError("Initial number of players must be two or more.")
            
        self.turns = 0
        
        players = {}
        for i in range(ni):
            players[i + 1] = player(i + 1)
            
        players[1].set_left_player(players[ni])
        players[1].set_right_player(players[2])
        self.player1 = players[1]
        
        for i in range(2, ni):
            players[i].set_left_player(players[i - 1])
            players[i].set_right_player(players[i + 1])
            
        players[ni].set_left_player(players[ni - 1])
        players[ni].set_right_player(players[1])
            
        
    def play(self):
        """
        Play game and return winner.  Play always begins with player 1.
        
        Outputs:
        (int) : index of player relative to player who rolls first
        """
        
        player_current = self.player1
        
        while player_current.left != player_current:
            self.turns += 1
            player_current.turn()
            player_current = player_current.right
        
        return player_current
    
