# -*- coding: utf-8 -*-

from numpy.random import randint

class player:
    
    def __init__(self, name):
        """
        Initialize player
        
        Inputs:
        name (int or str) : player identifier
        """
        
        self.name = name  # player identifier
        
        self.coins = 3  # number of coins
        
        self.left = None  # players to left and right
        self.right = None
        
        self.zero = False
        
    def roll_dice(self):
        """
        Roll dice and adjust coins for player
        """
        
        if self.coins == 0:
            return RuntimeError("Player must have coins in order to roll.")
            
        # loop through each dice
        for _ in range(min(self.coins, 3)):
            
            value = randint(6)
            
            if value < 3:  # safe
                pass
            else: # give coin
                self.coins -= 1  # to bank
                if value == 5:  # to left
                    self.left.coins += 1
                if value == 6:  # to right
                    self.right.coins += 1
                    
    def turn(self):
        """
        Simulate turn for player.  If no coins, eliminate player from game.
        """
        
        if not self.coins: # out of turns
            self.left.set_right_player(self.right)
            self.right.set_left_player(self.left)
        else:
            self.roll_dice()
            
    def set_left_player(self, playerObj):
        """
        Setter for left player
        
        Inputs:
        playerObj (player object) - play to left
        """
        self.left = playerObj
        
    def set_right_player(self, playerObj):
        """
        Setter for left player
        
        Inputs:
        playerObj (player object) - play to left
        """
        self.right = playerObj
            