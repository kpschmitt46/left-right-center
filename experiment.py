# -*- coding: utf-8 -*-
"""
Created on Sat Dec 23 22:45:09 2017

@author: Kyle Schmitt
"""

# imports
import time
import numpy as np
import matplotlib.pyplot as plt

from gameplayClass import gameplay

ngames = 10000  #  number of Monte Carlo trials
ni = 8  # number of players

# compile statistics for a game
t1 = time.time()
winners = []
coins = []
turns = []
for _ in range(ngames):
    gameplay_current = gameplay(ni)
    winner = gameplay_current.play()
    coins.append(winner.coins)
    winners.append(winner.name)
    turns.append(gameplay_current.turns)
dur = time.time() - t1
    
# plot results
plt.hist(winners, normed=True, bins=np.arange(0.5, ni + 0.6, 1.0), edgecolor='k', width=1.0)
plt.xlabel('Player')
plt.ylabel('Likelihood')
plt.show()

plt.hist(coins, normed=True, bins=np.arange(0.5, max(coins) + 0.6, 1.0), edgecolor='k', width=1.0)
plt.xlabel('Coins Held By Winner')
plt.ylabel('Likelihood')
plt.show()

plt.hist(turns, normed=True, bins=np.arange(max(min(turns) - 5.5, 0), max(turns) + 5.5, 1.0), edgecolor='k', width=1.0)
plt.xlabel('Turns in Game')
plt.ylabel('Likelihood')
plt.show()